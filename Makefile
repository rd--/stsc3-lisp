all:
	echo "stsc3-lisp"

clean:
	rm -Rf dist dist-newstyle cabal.project.local *~
	find . -name '*.o' -exec rm {} +
	find . -name '*.hi' -exec rm {} +

push-all:
	r.gitlab-push.sh stsc3-lisp

indent:
	fourmolu -i Interpreter cmd
