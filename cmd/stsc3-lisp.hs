import System.Environment {- base -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Ugen.Dot as Dot {- hsc3-dot -}

import qualified Interpreter.Lisp.Expr {- stsc3-lisp -}

stsc3_play :: (FilePath -> IO Sc3.Ugen) -> FilePath -> IO ()
stsc3_play evalFile fn = evalFile fn >>= Sc3.audition

stsc3_draw :: (FilePath -> IO Sc3.Ugen) -> FilePath -> IO ()
stsc3_draw evalFile fn = evalFile fn >>= Dot.draw . Sc3.out 0

help :: [String]
help =
  [ "stsc3 command [arguments]"
  , " {draw|play} file"
  , " repl"
  , " stop"
  ]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["repl"] -> Interpreter.Lisp.Expr.replMain
    ["draw", fn] -> stsc3_draw Interpreter.Lisp.Expr.evalSmalltalkFile fn
    ["play", fn] -> stsc3_play Interpreter.Lisp.Expr.evalSmalltalkFile fn
    ["stop"] -> Sc3.withSc3 Sc3.reset
    _ -> putStrLn (unlines help)
