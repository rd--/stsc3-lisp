# stsc3-lisp

Command line stsc3-lisp.

## repl

An interpreter for Smalltalk programs where the only
data type is the SuperCollider Unit Generator.

This interpreter can run the Smalltalk SuperCollider help graphs
directly, without requiring a Smalltalk system.

However it is _not_ a Smalltalk system, it is rather a dialect of Lisp
with Smalltalk syntax.  It is implemented using Haskell SuperCollider,
and inherits all of it's behaviour from that system.  Math at constant
UGens is optimised, so printing for constant expressions is ordinary.

~~~~
$ stsc3-lisp repl
1 + 2
result: 3.0
[:x | x * x] value: 4
result: 16.0
#(1 2 3) * 2
result: {2.0. 4.0. 6.0}
4 arrayFill: [:i | 5.0 rand]
result: {0.13. 2.61. 2.70. 1.44}
4 arrayFill: [:i | #(1 2 3 4 5) atRandom]
result: {4.0. 3.0. 2.0. 3.0}
#(#(1 2) #(3 4)) transpose concatenation
result: {1.0. 3.0. 2.0. 4.0}
^d
$
~~~~
