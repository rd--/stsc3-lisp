{- | Simple Smalltalk/Lisp expression interpreter abstracted over Block evaluator.
     The initial interpreter is over the Ansi Ast, c.f. Interpreter.Lisp.Direct
-}
module Interpreter.Lisp.Common where

import Control.Monad {- base -}
import Data.Bits {- base -}
import Data.Char {- base -}
import Data.List {- base -}
import System.IO {- base -}

import Control.Monad.Except {- mtl -}
import Control.Monad.State {- mtl -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Plain as Plain {- hsc3 -}

import qualified Sound.Sc3.Lang.Random.Io as Random {- hsc3-lang -}

import qualified Sound.Sc3.Ugen.Dot {- hsc3-dot -}

import qualified Interpreter.Som.Dict as Dict {- stsc3-som -}

import qualified Language.Sc3.Lisp.Env as Env {- hsc3-lisp -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Smalltalk as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}

import qualified Language.Smalltalk.Ansi as St {- stsc3 -}

type Name = String

-- | Object
data Object t
  = NilObject
  | ClassObject Name
  | UgenClassObject Name Db.U
  | UgenObject Sc3.Ugen
  | -- | There is no separate string type.
    SymbolObject String
  | BlockObject (Env.Env Name (Object t)) t

-- | State monad wrapped in Exception monad.
type EnvMonad m k v r = ExceptT String (StateT (Env.Env k v) m) r

-- | VM is simply an environment.
type VM o t = EnvMonad IO Name (Object o) t

type EvalBlock t = Bool -> Env.Env Name (Object t) -> t -> [Object t] -> VM t (Object t)

ugenShow :: Sc3.Ugen -> String
ugenShow x =
  case x of
    Sc3.Constant_U c -> show (Sc3.constantValue c)
    Sc3.Mce_U m -> "{" ++ intercalate ". " (map ugenShow (Sc3.mce_to_list m)) ++ "}"
    _ -> show x

instance Show (Object t) where
  show o =
    case o of
      NilObject -> "nil"
      SymbolObject x -> show x
      BlockObject _ _ -> "Block * *"
      UgenClassObject x _ -> x
      UgenObject x -> ugenShow x
      ClassObject x -> x

-- | Object to Ugen
objectToUgen :: Object t -> VM t Sc3.Ugen
objectToUgen o =
  case o of
    UgenObject x -> return x
    _ -> throwError "objectToUgen: Object not Ugen?"

-- | Object to list of Ugen
objectToMCE :: String -> Object t -> VM t [Sc3.Ugen]
objectToMCE msg o =
  case o of
    UgenObject x -> return (Sc3.mceChannels x)
    _ -> throwError ("objectToMCE: Object not Ugen: " ++ msg)

-- | Object to Int
objectToInt :: String -> Object t -> VM t Int
objectToInt msg o =
  case o of
    UgenObject x -> maybe (throwError ("objectToInt: " ++ msg)) (return . round) (Sc3.u_constant x)
    _ -> throwError ("objectToInt: Object not Ugen: " ++ msg)

-- | Object to Symbol
objectToSymbol :: Object t -> VM t String
objectToSymbol o =
  case o of
    SymbolObject x -> return x
    _ -> throwError "objectToSymbol: Object not symbol"

-- | Object to Block
objectToBlock :: Object t -> VM t (Env.Env Name (Object t), t)
objectToBlock o =
  case o of
    BlockObject e x -> return (e, x)
    _ -> throwError "objectToBlock: Object not Block?"

-- | Object to Double
objectToDouble :: Object t -> VM t Double
objectToDouble o =
  case o of
    UgenObject x -> maybe (throwError "objectToDouble") return (Sc3.u_constant x)
    _ -> throwError "objectToDouble: Object not Ugen?"

-- | Object to list of Object
objectToArray :: String -> Object t -> VM t [Object t]
objectToArray msg o = do
  u <- objectToMCE msg o
  return (map UgenObject u)

-- | Identifier (String) to Object
identifierToObject :: St.Identifier -> VM t (Object t)
identifierToObject x = return (SymbolObject x)

-- | Double to Object
doubleToObject :: Double -> Object t
doubleToObject x = UgenObject (Sc3.double_to_ugen x)

-- | Int to Object
intToObject :: Int -> Object t
intToObject x = UgenObject (Sc3.int_to_ugen x)

-- | List of Ugen to Object
mceToObject :: [Sc3.Ugen] -> Object t
mceToObject = UgenObject . Sc3.mce

-- | List of Object to Object
arrayToObject :: [Object t] -> VM t (Object t)
arrayToObject a = do
  u <- mapM objectToUgen a
  return (mceToObject u)

literalToObject :: St.Literal -> VM t (Object t)
literalToObject l =
  case l of
    St.NumberLiteral (St.Int x) -> return (UgenObject (Sc3.constant x))
    St.NumberLiteral (St.Float x) -> return (UgenObject (Sc3.constant x))
    St.StringLiteral x -> return (SymbolObject x) -- ?
    St.CharacterLiteral _ -> throwError "literalToObject: character?"
    St.SymbolLiteral x -> return (SymbolObject x)
    St.SelectorLiteral (St.UnarySelector "dinf") -> return (UgenObject Sc3.dinf)
    St.SelectorLiteral (St.UnarySelector x) -> return (SymbolObject x)
    St.SelectorLiteral _ -> throwError "literalToObject: selector?"
    St.ArrayLiteral x -> arrayToObject =<< (mapM (either literalToObject identifierToObject) x)

-- | Add temporaries as single frame to environment, initialised to nil.
evalTemporaries :: [St.Identifier] -> VM t ()
evalTemporaries x =
  put =<< liftIO . Env.envAddFrameFromList (zip x (repeat NilObject)) =<< get

-- | Delete frame and return input value.
deleteFrame :: r -> VM t r
deleteFrame r = (put =<< Env.envDeleteFrame =<< get) >> return r

isCapitalised :: String -> Bool
isCapitalised x =
  case x of
    c : _ -> isUpper c
    _ -> False

isUgenName :: String -> Bool
isUgenName x = x `elem` map Db.ugen_name Db.ugen_db

-- | Lookup identifier, which is either a Ugen Class name, or a Class name, or a value in the environment.
lookupIdentifier :: St.Identifier -> VM t (Object t)
lookupIdentifier x =
  if isUgenName x
    then return (UgenClassObject x (Db.u_lookup_cs_err x))
    else
      if isCapitalised x
        then return (ClassObject x)
        else get >>= \e -> Env.envLookup x e

extendEnvironment :: Env.Env Name e -> [(Name, e)] -> VM t (Env.Env Name e)
extendEnvironment e x = if null x then return e else liftIO (Env.envAddFrameFromList x e)

genUid :: VM t Sc3.UgenId
genUid = do
  x <- liftIO Sc3.generateUid
  return (Sc3.Uid x)

genRand :: Object t -> VM t (Object t)
genRand o = do
  x <- objectToDouble o
  fmap doubleToObject (liftIO (Random.rand x))

genRand2 :: Object t -> VM t (Object t)
genRand2 o = do
  x <- objectToDouble o
  fmap doubleToObject (liftIO (Random.rand2 x))

genRRand :: Object t -> Object t -> VM t (Object t)
genRRand o p1 = do
  x <- objectToDouble o
  y <- objectToDouble p1
  fmap doubleToObject (liftIO (Random.rrand x y))

genExpRand :: Object t -> Object t -> VM t (Object t)
genExpRand o p1 = do
  x <- objectToDouble o
  y <- objectToDouble p1
  fmap doubleToObject (liftIO (Random.exprand x y))

asLocalBuf :: Object t -> VM t (Object t)
asLocalBuf aUgen = do
  uid <- liftIO Sc3.generateUid
  u <- objectToMCE "asLocalBuf" aUgen
  return (UgenObject (Sc3.asLocalBufId uid u))

mceConcatenation :: Object t -> VM t (Object t)
mceConcatenation aUgen = do
  u <- objectToMCE "mceConcatenation" aUgen
  let m = map Sc3.mceChannels u
  return (mceToObject (concat m))

evalUnaryUgenMessage :: Object t -> String -> VM t (Object t)
evalUnaryUgenMessage o m = do
  x <- objectToUgen o
  case m of
    "abs" -> liftUgen abs o
    "acos" -> liftUgen acos o
    "acosh" -> liftUgen acosh o
    "ar" -> liftUgen (Sc3.rewriteToRate Sc3.ar) o
    "asArray" -> return o
    "asFloat" -> return o
    "asLocalBuf" -> asLocalBuf o
    "asin" -> liftUgen asin o
    "asinh" -> liftUgen asinh o
    "atRandom" -> objectToArray "atRandom" o >>= Random.choose
    "atan" -> liftUgen atan o
    "atanh" -> liftUgen atanh o
    "ceil" -> liftUgen Sc3.ceil o
    "concatenation" -> mceConcatenation o
    "constant" -> if Sc3.isConstant x then return o else throwError "evalUnaryUgenMessage: constant?"
    "cos" -> liftUgen cos o
    "cosh" -> liftUgen cosh o
    "cubed" -> liftUgen Sc3.cubed o
    "distort" -> liftUgen Sc3.distort o
    "dr" -> liftUgen (Sc3.rewriteToRate Sc3.dr) o
    "draw" -> liftIO (Sound.Sc3.Ugen.Dot.draw x) >> return NilObject
    "exp" -> liftUgen exp o
    "floor" -> liftUgen Sc3.floorE o
    "frac" -> liftUgen Sc3.frac o
    "ir" -> liftUgen (Sc3.rewriteToRate Sc3.ir) o
    "kr" -> liftUgen (Sc3.rewriteToRate Sc3.kr) o
    "log" -> liftUgen log o
    "max" -> objectToArray "max" o >>= mapM objectToDouble >>= return . doubleToObject . maximum
    "mce" -> return o
    "midicps" -> liftUgen Sc3.midiCps o
    "mix" -> liftUgen Sc3.mix o
    "negated" -> liftUgen negate o
    "play" -> liftIO (Sc3.audition x) >> return NilObject
    "rand" -> genRand o
    "rand2" -> genRand2 o
    "reciprocal" -> liftUgen recip o
    "round" -> liftUgen Sc3.roundE o
    "sin" -> liftUgen sin o
    "sinh" -> liftUgen sinh o
    "size" -> fmap (intToObject . length) (objectToMCE "size" o)
    "sqrt" -> liftUgen sqrt o
    "tan" -> liftUgen tan o
    "tanh" -> liftUgen tanh o
    "transpose" -> liftUgen Sc3.mceTranspose o
    _ -> throwError ("evalUnaryUgenMessage: " ++ m)

evalBinaryUgenMessage :: Object t -> String -> Object t -> VM t (Object t)
evalBinaryUgenMessage o m rhs = do
  case (o, rhs) of
    (UgenObject x, UgenObject y) ->
      case m of
        "+" -> return (UgenObject (x + y))
        "-" -> return (UgenObject (x - y))
        "*" -> return (UgenObject (x * y))
        "/" -> return (UgenObject (x / y))
        "%" -> return (UgenObject (x `Sc3.modE` y))
        ">" -> return (UgenObject (x `Sc3.greater_than` y))
        ">=" -> return (UgenObject (x `Sc3.greater_than_or_equal_to` y))
        "<" -> return (UgenObject (x `Sc3.less_than` y))
        "<=" -> return (UgenObject (x `Sc3.less_than_or_equal_to` y))
        "==" -> return (UgenObject (x `Sc3.equal_to` y))
        "," -> return (mceToObject (concat (map Sc3.mceChannels [x, y])))
        _ -> throwError ("evalBinaryMessage: Ugen: " ++ m)
    _ -> throwError ("evalBinaryMessage: " ++ show (m, o, rhs))

makeUgen :: String -> Sc3.Rate -> [Object t] -> Int -> Sc3.UgenId -> [Object t] -> Bool -> VM t (Object t)
makeUgen ugenName ugenRate ugenInputObjects ugenNumChan ugenId optInputObjects mceInputs = do
  ugenInputs <- mapM objectToUgen ugenInputObjects
  optInputs <- mapM objectToUgen optInputObjects
  let plainInputs = (if mceInputs then Sc3.halt_mce_transform else id) ugenInputs
      u = Plain.mk_plain ugenRate ugenName plainInputs ugenNumChan (Sc3.Special 0) ugenId
      o = Sc3.ugen_optimise_const_operator u
  m <- case optInputs of
    [] -> return o
    [mul, add] -> return (Sc3.mulAdd o mul add)
    [mul] -> return (o * mul)
    _ -> throwError "makeUgen: optInputs?"
  return (UgenObject (Sc3.ugen_optimise_const_operator m))

ugenRequiredKeywordNames :: Db.U -> [String]
ugenRequiredKeywordNames = map (\x -> x ++ ":") . Db.st_gen_required_arg

intReplicate :: Object t -> Object t -> VM t (Object t)
intReplicate p1 p2 = do
  i <- objectToInt "intReplicate" p1
  u <- objectToUgen p2
  return (mceToObject (replicate i u))

-- | This culls the argument to evalBlock, i.e. allow blocks of zero arguments.
mceFill :: EvalBlock t -> Bool -> (Sc3.Ugen -> Sc3.Ugen) -> Object t -> Object t -> VM t (Object t)
mceFill evalBlock zeroIndexed f k blockObject = do
  i <- objectToInt "mceFill" k
  (e, b) <- objectToBlock blockObject
  let j = map intToObject (if zeroIndexed then [0 .. i - 1] else [1 .. i])
  a <- mapM (\x -> evalBlock True e b [x]) j
  u <- mapM objectToUgen a
  return (UgenObject (f (Sc3.mce u)))

mceDo :: EvalBlock t -> Object t -> Object t -> VM t (Object t)
mceDo evalBlock o aBlock = do
  a <- objectToArray "mceDo" o
  case aBlock of
    BlockObject env blockBody -> mapM_ (evalBlock False env blockBody . return) a >> return NilObject
    _ -> throwError "mceDo"

mceInjectInto :: EvalBlock t -> Object t -> Object t -> Object t -> VM t (Object t)
mceInjectInto evalBlock o aValue aBlock = do
  u <- objectToMCE "mceInjectInto" o
  case aBlock of
    BlockObject env blockBody -> foldM (\i j -> evalBlock False env blockBody [i, j]) aValue (map UgenObject u)
    _ -> throwError "mceInjectInto?"

mceCollect :: EvalBlock t -> Object t -> Object t -> VM t (Object t)
mceCollect evalBlock o aBlock = do
  x <- objectToUgen o
  case aBlock of
    BlockObject env blockBody -> arrayToObject =<< (mapM (evalBlock False env blockBody . return . UgenObject) (Sc3.mceChannels x))
    _ -> throwError "mceCollect?"

mceWithCollect :: EvalBlock t -> Object t -> Object t -> Object t -> VM t (Object t)
mceWithCollect evalBlock o aUgen aBlock = do
  x <- objectToUgen o
  y <- objectToUgen aUgen
  let m = transpose [Sc3.mceChannels x, Sc3.mceChannels y]
  case aBlock of
    BlockObject env blockBody -> arrayToObject =<< (mapM (evalBlock False env blockBody . map UgenObject) m)
    _ -> throwError "mceWithCollect?"

intToDo :: EvalBlock t -> Object t -> Object t -> Object t -> VM t (Object t)
intToDo evalBlock p1 p2 aBlock = do
  i1 <- objectToInt "intToDo" p1
  i2 <- objectToInt "intToDo" p2
  let a = map intToObject [i1 .. i2]
  case aBlock of
    BlockObject env blockBody -> mapM_ (evalBlock False env blockBody . return) a >> return NilObject
    _ -> throwError "intToDo?"

liftUgen :: (Sc3.Ugen -> Sc3.Ugen) -> Object t -> VM t (Object t)
liftUgen f p1 = objectToUgen p1 >>= return . UgenObject . f

liftUgen2 :: (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen) -> Object t -> Object t -> VM t (Object t)
liftUgen2 f p1 p2 = do
  u1 <- objectToUgen p1
  u2 <- objectToUgen p2
  return (UgenObject (f u1 u2))

liftUgen3 :: (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen) -> Object t -> Object t -> Object t -> VM t (Object t)
liftUgen3 f p1 p2 p3 = do
  u1 <- objectToUgen p1
  u2 <- objectToUgen p2
  u3 <- objectToUgen p3
  return (UgenObject (f u1 u2 u3))

liftUgen4 :: (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen) -> Object t -> Object t -> Object t -> Object t -> VM t (Object t)
liftUgen4 f p1 p2 p3 p4 = do
  u1 <- objectToUgen p1
  u2 <- objectToUgen p2
  u3 <- objectToUgen p3
  u4 <- objectToUgen p4
  return (UgenObject (f u1 u2 u3 u4))

{-
liftUgen5 :: (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen) -> Object t -> Object t -> Object t -> Object t -> Object t -> VM t (Object t)
liftUgen5 f p1 p2 p3 p4 p5 = do
  u1 <- objectToUgen p1
  u2 <- objectToUgen p2
  u3 <- objectToUgen p3
  u4 <- objectToUgen p4
  u5 <- objectToUgen p5
  return (UgenObject (f u1 u2 u3 u4 u5))

liftUgen6 :: (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen) -> Object t -> Object t -> Object t -> Object t -> Object t -> Object t -> VM t (Object t)
liftUgen6 f p1 p2 p3 p4 p5 p6 = do
  u1 <- objectToUgen p1
  u2 <- objectToUgen p2
  u3 <- objectToUgen p3
  u4 <- objectToUgen p4
  u5 <- objectToUgen p5
  u6 <- objectToUgen p6
  return (UgenObject (f u1 u2 u3 u4 u5 u6))
-}

arrayFromToBy :: Object t -> Object t -> Object t -> VM t (Object t)
arrayFromToBy p1 p2 p3 = do
  i <- objectToInt "arrayFromToBy" p1
  j <- objectToInt "arrayFromToBy" p2
  k <- objectToInt "arrayFromToBy" p3
  return (mceToObject (map Sc3.int_to_ugen [i, i + k .. j]))

arrayFromTo :: Object t -> Object t -> VM t (Object t)
arrayFromTo p1 p2 = arrayFromToBy p1 p2 (intToObject 1)

ifTrueIfFalse :: EvalBlock t -> Object t -> Object t -> Object t -> VM t (Object t)
ifTrueIfFalse evalBlock p1 p2 p3 = do
  aBool <- objectToInt "ifTrueIfFalse" p1
  (e1, b1) <- objectToBlock p2
  (e2, b2) <- objectToBlock p3
  if aBool /= 0 then evalBlock False e1 b1 [] else evalBlock False e2 b2 []

controlInput :: Object t -> Object t -> VM t (Object t)
controlInput p1 p2 = do
  nm <- objectToSymbol p1
  df <- objectToDouble p2
  return (UgenObject (Sc3.control Sc3.kr nm df))

mceAt :: Object t -> Object t -> VM t (Object t)
mceAt o p1 = do
  u <- objectToUgen o
  i <- objectToInt "mceAt" p1
  return (UgenObject (Sc3.mceChannel (i - 1) u))

envGen :: Object t -> Object t -> Object t -> VM t (Object t)
envGen o p1 p2 = makeUgen "EnvGen" Sc3.ar [p1, doubleToObject 1, doubleToObject 0, doubleToObject 1, p2, o] 1 Sc3.NoId [] True

evalKeywordClassMessage :: EvalBlock t -> String -> [(String, Object t)] -> VM t (Object t)
evalKeywordClassMessage evalBlock x keywordArguments = do
  case (x, keywordArguments) of
    ("Control", [("name:", p1), ("init:", p2)]) -> controlInput p1 p2
    ("EnvSine", [("dur:", p1), ("level:", p2)]) -> envSine p1 p2
    ("Interval", [("from:", p1), ("to:", p2)]) -> arrayFromTo p1 p2
    ("Interval", [("from:", p1), ("to:", p2), ("by:", p3)]) -> arrayFromToBy p1 p2 p3
    ("MRG", [("lhs:", p1), ("rhs:", p2)]) -> liftUgen2 Sc3.mrg2 p1 p2
    ("OverlapTexture", [("graphFunc:", p1), ("sustainTime:", p2), ("transitionTime:", p3), ("overlap:", p4)]) ->
      overlapTexture evalBlock p1 p2 p3 p4
    ("Splay", [("input:", p1)]) -> liftUgen (\input -> Sc3.splay input 1 1 0 True) p1
    ("Splay", [("input:", p1), ("level:", p2)]) -> liftUgen2 (\input level -> Sc3.splay input 1 level 0 True) p1 p2
    ("Splay", [("input:", p1), ("spread:", p2), ("level:", p3), ("center:", p4)]) ->
      liftUgen4 (\p1' p2' p3' p4' -> Sc3.splay p1' p2' p3' p4' True) p1 p2 p3 p4
    ("TChoose", [("trig:", p1), ("array:", p2)]) -> tChoose p1 p2
    ("TXLine", [("start:", p1), ("end:", p2), ("dur:", p3), ("trig:", p4)]) -> tXLine p1 p2 p3 p4
    _ -> throwError ("evalKeywordMessage: ClassObject: " ++ x)

evalKeywordUgenMessage :: EvalBlock t -> Object t -> [(String, Object t)] -> VM t (Object t)
evalKeywordUgenMessage evalBlock o keywordArguments =
  case keywordArguments of
    [("arrayFill:", p1)] -> mceFill evalBlock False id o p1 -- synonym for mceFill:
    [("arrayFillZeroIndexed:", p1)] -> mceFill evalBlock True id o p1 -- synonym for mceFillZeroIndexed:
    [("at:", p1)] -> mceAt o p1
    [("bitAnd:", p1)] -> liftUgen2 (.&.) o p1
    [("bitOr:", p1)] -> liftUgen2 (.|.) o p1
    [("bitShiftRight:", p1)] -> liftUgen2 Sc3.shiftRight o p1
    [("clip2:", p1)] -> liftUgen2 Sc3.clip2 o p1
    [("clump:", p1)] -> objectToInt "clump:" p1 >>= \k -> liftUgen (Sc3.mceClump k) o
    [("collect:", p1)] -> mceCollect evalBlock o p1
    [("do:", p1)] -> mceDo evalBlock o p1
    [("envGen:", p1), ("doneAction:", p2)] -> envGen o p1 p2
    [("exprand:", p1)] -> genExpRand o p1
    [("gcd:", p1)] -> liftUgen2 Sc3.gcdE o p1
    [("ifTrue:", p1), ("ifFalse:", p2)] -> ifTrueIfFalse evalBlock o p1 p2
    [("inExpRangeFrom:", p1), ("to:", p2)] -> liftUgen3 (\u1 u2 u3 -> Sc3.linExp u1 (-1) 1 u2 u3) o p1 p2
    [("inRangeFrom:", p1), ("to:", p2)] -> liftUgen3 (\u1 u2 u3 -> Sc3.linLin u1 (-1) 1 u2 u3) o p1 p2
    [("inject:", p1), ("into:", p2)] -> mceInjectInto evalBlock o p1 p2
    [("lcm:", p1)] -> liftUgen2 Sc3.lcmE o p1
    [("max:", p1)] -> liftUgen2 max o p1
    [("min:", p1)] -> liftUgen2 min o p1
    [("mceFill:", p1)] -> mceFill evalBlock False id o p1
    [("mceFillZeroIndexed:", p1)] -> mceFill evalBlock True id o p1
    [("mixFill:", p1)] -> mceFill evalBlock False Sc3.mix o p1
    [("mixFillZeroIndexed:", p1)] -> mceFill evalBlock True Sc3.mix o p1
    [("mul:", p1), ("add:", p2)] -> liftUgen3 Sc3.mulAdd o p1 p2
    [("raisedTo:", p1)] -> liftUgen2 (**) o p1
    [("rand:", p1)] -> genRRand o p1
    [("replicate:", p1)] -> intReplicate o p1
    [("round:", p1)] -> liftUgen2 Sc3.roundTo o p1
    [("to:", p1), ("by:", p2)] -> arrayFromToBy o p1 p2
    [("to:", p1), ("do:", p2)] -> intToDo evalBlock o p1 p2
    [("to:", p1)] -> arrayFromTo o p1
    [("with:", p1), ("collect:", p2)] -> mceWithCollect evalBlock o p1 p2
    _ -> throwError ("evalKeywordMessage: Ugen: " ++ show (map fst keywordArguments))

{- | Where o is a Sc3.Ugen class:
     check keyword names match Sc3.Ugen input names,
     check for mulAdd inputs,
     check for numChan input,
     derive number of output channels and rate,
     check if Ugen is non-determinate and if so generate Uid,
-}
evalKeywordUgenClassMessage :: String -> Db.U -> [(String, Object t)] -> VM t (Object t)
evalKeywordUgenClassMessage x u keywordArguments = do
  let (keywordNames, keywordValues) = unzip keywordArguments
      requiredKeywords = ugenRequiredKeywordNames u
      numRequiredKeywords = length requiredKeywords
      optKeywords = drop numRequiredKeywords keywordNames
      (requiredValues, optValues) = splitAt numRequiredKeywords keywordValues
  when
    (not (keywordNames `isPrefixOf` requiredKeywords) && not (optKeywords `isPrefixOf` ["mul:", "add:"]))
    (throwError "evalKeywordMessage: incorrect keyword message?")
  uid <- if Db.ugen_nondet u then genUid else return Sc3.NoId
  let rt = Db.ugen_default_rate u
  (nc, inputValues) <-
    if x == "Demand"
      then fmap (\i -> (i, requiredValues)) (fmap (length . Sc3.mceChannels) (objectToUgen (last requiredValues)))
      else case Db.ugen_outputs u of
        Just fixedNumChan -> return (fixedNumChan, requiredValues)
        Nothing -> fmap (\varNumChan -> (varNumChan, tail requiredValues)) (objectToInt "numChan" (requiredValues !! 0))
  ugen <- makeUgen x rt inputValues nc uid optValues (Db.ugen_std_mce u > 0)
  return ugen

overlapTexture :: EvalBlock t -> Object t -> Object t -> Object t -> Object t -> VM t (Object t)
overlapTexture evalBlock graphFunc sustainTime transitionTime overlap = do
  (e, b) <- objectToBlock graphFunc
  t1 <- objectToUgen sustainTime
  t2 <- objectToUgen transitionTime
  k <- objectToInt "overlapTexture" overlap
  let tr_seq = map (\i -> Sc3.impulse Sc3.kr (1 / (t1 + (t2 * 2))) (Sc3.constant i / Sc3.constant k)) [0 .. k - 1]
      en_seq = map (\tr -> Sc3.envGen Sc3.kr tr 1 0 1 Sc3.DoNothing (Sc3.envelope [0, 1, 1, 0] [t1, t2, t1] [Sc3.EnvSin])) tr_seq
  a <- mapM (\x -> evalBlock False e b [x]) (map UgenObject tr_seq)
  u <- mapM objectToUgen a
  return (UgenObject (Sc3.mix (Sc3.mce (zipWith (*) u en_seq))))

tChoose :: Object t -> Object t -> VM t (Object t)
tChoose p1 p2 = do
  z <- liftIO Sc3.generateUid
  liftUgen2 (Sc3.tChooseId z) p1 p2

tXLine :: Object t -> Object t -> Object t -> Object t -> VM t (Object t)
tXLine p1 p2 p3 p4 = liftUgen4 (Sc3.tXLine Sc3.ar) p1 p2 p3 p4

envSine :: Object t -> Object t -> VM t (Object t)
envSine p1 p2 = do
  u1 <- objectToUgen p1
  u2 <- objectToUgen p2
  return (UgenObject (Sc3.envelope_to_ugen (Sc3.envSine u1 u2)))

coreDict :: MonadIO m => m (Dict.Dict Name (Object t))
coreDict =
  Dict.dictFromList
    [ ("true", UgenObject (Sc3.int_to_ugen 1))
    , ("false", UgenObject (Sc3.int_to_ugen 0))
    ]

getProgram :: String -> Handle -> IO String
getProgram s h = do
  l <- hGetLine h -- no eol
  r <- hReady h
  let s' = s ++ (l ++ "\n")
  if r then getProgram s' h else return s'

initialEnvironment :: IO (Env.Env Name (Object t))
initialEnvironment = coreDict >>= Env.envNewFrom
