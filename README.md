# stsc3-lisp

Simple Lisp-like interpreter for a subset of
[Smalltalk](http://archive.org/details/byte-magazine-1981-08/)
syntax using the parser and abstract syntax tree of
[sts3c](http://rohandrape.net/?t=stsc3).

The only data type is the
[SuperCollider](http://audiosynth.com/)
_unit generator_.

![](https://rohandrape.net/sw/stsc3/lib/png/smalltalk-balloon.png)

Cli:
[stsc3-lisp](https://rohandrape.net/?t=stsc3-lisp&e=md/stsc3-lisp.md)

© [Rohan Drape](http://rohandrape.net/), 2019-2023, [Gpl](http://gnu.org/copyleft/)
